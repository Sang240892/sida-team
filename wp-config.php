<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sida');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^/^nT*m|d25]o|D3-MW:i+U;hZjg-`IMQfm2aqGW:_o.By&[x#JGs5c6J!AYGQps');
define('SECURE_AUTH_KEY',  'h51%gv{&2QC )l]-z:.hYEvr!b@AZlN&&(tHF?SZDMp%E>|b#hw0ODL/eYI|RZ2G');
define('LOGGED_IN_KEY',    'K{T=7KqLeiJ-,p91uSHXU|J+$w|=J-XNgu&Pj@9slrgV5N-k*EH}#}< ?.Qk[@E)');
define('NONCE_KEY',        'm1O!5egt/iKw*6{]? [A+9n.|&Gr JV`xO;u<#b6!QP+J~-{-mtKiCjY<2#KS|j,');
define('AUTH_SALT',        '*L3+&7nJ`%<@#=mQ|ATPv+EX<]:rjVl)[[V50OXQS/2VjM-%RM|OiKaL9e3.:(pn');
define('SECURE_AUTH_SALT', '(KULP[V+>T^x78+0a}UsyN]d~F#sMa-r^)%nSe/_E_-IW:U3y$-u#k@iUd!A?^+d');
define('LOGGED_IN_SALT',   '-AVrY(!|J8Jqs]vKyYW&p>gV<%LY`_NyN+)Co0,aP~{^@TlAT|lrFW68iaX>}DC{');
define('NONCE_SALT',       'SD0*MJC-yC%eM- ZD)nF@#N|x~=BO!5^-;h}Z]+?3s2Cx-r7|.WjP_O6nIEl+6d{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
