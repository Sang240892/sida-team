<?php

	global $wp_customize;
	
	$zerif_events_category_show = get_theme_mod('zerif_events_category_show');

	zerif_before_events_category_trigger();

	if( !empty($zerif_events_category_show) ):
	
		echo '<section class="events_category" id="events_category">';
	
	elseif( isset( $wp_customize ) ):

		echo '<section class="events_category zerif_hidden_if_not_customizer" id="events_category">';
		
	endif;

	zerif_top_events_category_trigger();

	if( !empty($zerif_events_category_show) || isset( $wp_customize ) ):

		echo '<div class="container">
                         <div class="section-header">';

			$zerif_events_category_title = get_theme_mod('zerif_events_category_title','EVENTS');
			$zerif_events_category_subtitle = get_theme_mod('zerif_events_category_subtitle','Lorem ip sum');
			
			if( !empty($zerif_events_category_title) ):
			
				echo '<h2 class="white-text">'.$zerif_events_category_title.'</h2>';
			
			elseif ( isset( $wp_customize ) ):
			
				echo '<h2 class="white-text zerif_hidden_if_not_customizer"></h2>';
				
			endif;
			
			if( !empty($zerif_events_category_subtitle) ):
			
				echo '<div class="sub-heading white-text">'.$zerif_events_category_subtitle.'</div>';
				
			elseif ( isset( $wp_customize ) ):

				echo '<div class="sub-heading white-text zerif_hidden_if_not_customizer"></div>';
				
			endif;
			
			if(is_active_sidebar( 'sidebar-events' )):
				dynamic_sidebar( 'sidebar-events' );
			endif;

		echo '</div></div> <!-- / END CONTAINER -->';

		zerif_bottom_events_category_trigger();
		
	echo '</section> <!-- / END EVENTS SECTION -->';
	
	endif;

	zerif_after_events_category_trigger();